# Hi Reviewer!

Just run this `git clone https://gitlab.com/adamgen/users-orders.git && cd users-orders && npm run result`

It should clone and run the code if you run it on an empty directory and have angular cli installed.

## Summary
1. has two views: `users` and `users orders`
2. has crud on each list
3. edit and deletion is done from a separate component the display
4. adding done from the same (it's a calculated exception) 
5. has navigation that uses angular router
6. data stored on `window.localStorage` in a primitive/naive way
7. interactions with the data is done from a service
8. when choosing a customer you can see it's orders
9. orders components receives the user id as a parameter (it's the same orders from section 1 reused)
10. all in angualr 6 using cli

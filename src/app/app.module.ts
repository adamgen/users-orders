import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatExpansionModule } from '@angular/material';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';

import { AppComponent } from './app.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { RoutesModule } from './routes/routes.module';
import { UsersOrdersService } from './users-orders.service';
import { UsersComponent } from './components/users/users.component';
import { OrdersComponent } from './components/orders/orders.component';
import { EditUserDialogComponent } from './components/edit-user-dialog/edit-user-dialog.component';
import { EditOrderDialogComponent } from './components/edit-order-dialog/edit-order-dialog.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ConfirmDialogComponent } from './components/confirm-dialog/confirm-dialog.component';


@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
    UsersComponent,
    OrdersComponent,
    EditUserDialogComponent,
    EditOrderDialogComponent,
    ConfirmDialogComponent,
  ],
  imports: [
    BrowserModule,
    RoutesModule,
    MatExpansionModule,
    MatInputModule,
    BrowserAnimationsModule,
    MatChipsModule,
    MatIconModule,
    MatDialogModule,
    MatListModule,
    ReactiveFormsModule,
    MatButtonModule,
  ],
  entryComponents: [
    EditUserDialogComponent,
    EditOrderDialogComponent,
    ConfirmDialogComponent,
  ],
  providers: [UsersOrdersService],
  bootstrap: [AppComponent],
})
export class AppModule {}

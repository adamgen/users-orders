import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NotFoundComponent } from '../components/not-found/not-found.component';
import { UsersComponent } from '../components/users/users.component';
import { OrdersComponent } from '../components/orders/orders.component';

export const routes = [
  {
    path: 'users',
    component: UsersComponent,
  },
  {
    path: 'orders',
    component: OrdersComponent,
  },
  {
    path: 'orders/:user-id',
    component: OrdersComponent,
  },
  {
    path: '',
    redirectTo: '/users',
    pathMatch: 'full',
  },
  {
    path: '**',
    component: NotFoundComponent,
  },
];

@NgModule({
  imports: [
    // CommonModule,
    RouterModule.forRoot(routes),
  ],
  exports: [
    RouterModule,
  ],
  declarations: [],
})
export class RoutesModule {}

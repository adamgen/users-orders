import { Component } from '@angular/core';
import * as _ from 'lodash';
import { routes } from './routes/routes.module';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  routes: string[];

  constructor() {
    this.routes = _.chain(routes)
      .filter(route => _.get(route.path.match(/[\w-]+/g), '0', '') === route.path)
      .map(route => route.path)
      .value();
  }
}

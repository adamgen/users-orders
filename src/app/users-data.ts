export const usersData = [{
  name: 'Adam',
  id: 'ecc7c7a9-18b3-d0cb-df97-5c2c69db7df7',
  orders: [{ name: 'Bike', id: '8e704ad9-9bb5-da38-4324-517c874ef262' }, {
    name: 'Books',
    id: '229fd243-6cd6-1e04-900f-573cfa175dfb',
  }, { name: 'Macbook', id: '69d05e4d-9de6-d952-370f-6707ba37ff86' }],
}, {
  name: 'Rachel',
  id: '56441beb-3613-515a-97a0-43bbcb5520fa',
  orders: [{ name: 'Phone', id: 'a5375be6-2def-3834-6c40-c38e1caa19a6' }, {
    name: 'T Shirt',
    id: 'c0295326-0262-3230-e898-933a670d4274',
  }, { name: 'Mouse', id: '9ba8f725-fe34-964e-b58d-3dcd7ce30327' }],
}, {
  name: 'Avi',
  id: '7f3421b1-da6b-a962-fca2-5478ff53b692',
  orders: [{ name: 'Car', id: '80382985-d128-8e59-4340-02d0c10b702e' }, {
    name: 'Home',
    id: 'af3f1c8c-6170-1e73-df43-85f762702e91',
  }, { name: 'PC', id: '3b6d0fa4-18e7-e77e-7fb3-a83144a27713' }],
}, { name: 'Moshe', id: '98f97805-90db-3608-0554-cd638e6ffe89' }, {
  name: 'Avraham',
  id: '7440ad1b-4a80-75a0-4168-c216c7e470d9',
}];

import { Component, OnInit } from '@angular/core';
import { IUser, UsersOrdersService } from '../../users-orders.service';
import { MatDialog } from '@angular/material';
import { EditUserDialogComponent } from '../edit-user-dialog/edit-user-dialog.component';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
})
export class UsersComponent implements OnInit {
  addUserForm = new FormGroup({
    name: new FormControl(),
  });

  get users() {
    return this.usersOrdersService.users;
  }

  constructor(
    private usersOrdersService: UsersOrdersService,
    public dialog: MatDialog,
  ) { }

  ngOnInit() { }

  openDialog(user: IUser): void {
    const dialogRef = this.dialog.open(EditUserDialogComponent, {
      width: '750px',
      data: user,
    });
  }

  addUser() {
    if (this.addUserForm.value.name.length < 1) {
      return;
    }
    this.usersOrdersService.addUser(this.addUserForm.value.name);
    this.addUserForm.get('name').patchValue('');
  }
}

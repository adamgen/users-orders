import { Component, OnInit } from '@angular/core';
import { IOrder, IUser, UsersOrdersService } from '../../users-orders.service';
import { MatDialog } from '@angular/material';
import { EditOrderDialogComponent } from '../edit-order-dialog/edit-order-dialog.component';
import { ActivatedRoute } from '@angular/router';
import * as _ from 'lodash';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css'],
})
export class OrdersComponent implements OnInit {

  users = this.usersOrdersService.users;

  constructor(
    public usersOrdersService: UsersOrdersService,
    public dialog: MatDialog,
    public route: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      if (params['user-id']) {
        this.usersOrdersService.setExpandedUser(params['user-id']);
        this.users = _.filter(this.users, user => user.id === params['user-id']);
      }
    });
  }


  openDialog(user: IUser, order: IOrder): void {
    const dialogRef = this.dialog.open(EditOrderDialogComponent, {
      width: '750px',
      data: { user, order },
    });
  }

  add(userId: string, event: { input: HTMLInputElement, value: string }) {
    const input = event.input;
    this.usersOrdersService.addOrder(userId, event.value);

    if (input) { input.value = ''; }
  }
}

import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { UsersOrdersService } from '../../users-orders.service';

export interface IConfirmData {
  description: string;
  yes: any;
  no: any;
}

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.css'],
})
export class ConfirmDialogComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<ConfirmDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IConfirmData,
    public usersOrdersService: UsersOrdersService,
  ) {}


  ngOnInit() {}

  yes() {
    this.data.yes();
    this.dialogRef.close();
  }

  no() {
    this.data.no();
    this.dialogRef.close();
  }
}

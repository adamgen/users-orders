import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { IUser, UsersOrdersService } from '../../users-orders.service';
import { EditOrderDialogComponent } from '../edit-order-dialog/edit-order-dialog.component';
import { FormControl, FormGroup } from '@angular/forms';
import { ConfirmDialogComponent, IConfirmData } from '../confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-edit-user-dialog',
  templateUrl: './edit-user-dialog.component.html',
  styleUrls: ['./edit-user-dialog.component.css'],
})
export class EditUserDialogComponent implements OnInit {
  form = new FormGroup({
    name: new FormControl(this.data.name),
  });

  constructor(
    public dialogRef: MatDialogRef<EditOrderDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IUser,
    public usersOrdersService: UsersOrdersService,
    public dialog: MatDialog,
  ) {}

  ngOnInit() { }

  changed() {
    this.usersOrdersService.updateUser({ ...this.data, ...{ name: this.form.value.name } });
  }

  delete() {

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '750px',
      data: {
        description: `delete user ${this.form.value.name}`,
        yes: () => {
          this.usersOrdersService.deleteUser(this.data.id);
          this.dialogRef.close();
        },
        no: () => {},
      }as IConfirmData,
    });
  }
}

import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { IOrder, IUser, UsersOrdersService } from '../../users-orders.service';
import { FormControl, FormGroup } from '@angular/forms';
import { ConfirmDialogComponent, IConfirmData } from '../confirm-dialog/confirm-dialog.component';

interface IEditOrderData {
  order: IOrder;
  user: IUser;
}

@Component({
  selector: 'app-edit-order-dialog',
  templateUrl: './edit-order-dialog.component.html',
  styleUrls: ['./edit-order-dialog.component.css'],
})
export class EditOrderDialogComponent implements OnInit {
  form = new FormGroup({
    name: new FormControl(this.data.order.name),
  });

  constructor(
    public dialogRef: MatDialogRef<EditOrderDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IEditOrderData,
    public usersOrdersService: UsersOrdersService,
    public dialog: MatDialog,
  ) {}

  ngOnInit() { }

  changed() {
    this.usersOrdersService.updateOrder(this.data.user.id, { ...this.data.order, ...{ name: this.form.value.name } });
  }

  delete() {

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '750px',
      data: {
        description: `delete user ${this.form.value.name}`,
        yes: () => {
          this.usersOrdersService.deleteOrder(this.data.user.id, this.data.order.id);
          this.dialogRef.close();
        },
        no: () => {},
      }as IConfirmData,
    });
  }
}

import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { BehaviorSubject } from 'rxjs/index';
import { usersData } from './users-data';

function guid() {
  const s4 = () => Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

export interface IOrder {
  id: string;
  name: string;
}

export interface IUser {
  id: string;
  name: string;
  orders?: IOrder[];
}


@Injectable()
export class UsersOrdersService {
  get users() {
    return this.usersSubject.getValue();
  };

  set users(data) {
    this.usersSubject.next(data);
  };

  usersSubject = new BehaviorSubject(usersData);

  constructor() {
    this.getData();
    this.users = this.users || [];
    this.usersSubject.subscribe(data => {
      console.log(data);
    });
  }

  addUser(name: string) {
    this.users.push({ name, id: guid() });
    this.storeData();
  }

  addOrder(userId: string, name: string) {
    const user = _.find(this.users, getUser => getUser.id === userId);
    console.log(user.orders);
    user.orders = user.orders || [];
    user.orders.push({ name, id: guid() });
    this.storeData();
  }

  deleteUser(userId: string) {
    _.remove(this.users, user => user.id === userId);
    this.storeData();
  }

  deleteOrder(userId: string, orderId: string) {
    const user = _.find(this.users, getUser => getUser.id === userId);
    _.remove(user.orders, (order: IOrder) => order.id === orderId);
    this.storeData();
  }

  updateUser(updateUser: IUser) {
    const user = _.find(this.users, getUser => getUser.id === updateUser.id);
    Object.assign(user, updateUser);
    this.storeData();
  }

  updateOrder(userId: string, updateOrder: IOrder) {
    const user = _.find(this.users, getUser => getUser.id === userId);
    const order = _.find(user.orders, getOrder => getOrder.id === updateOrder.id);
    Object.assign(order, updateOrder);
    this.storeData();
  }

  isExpandedUser(userId: string) {
    return window.localStorage.getItem('expanded') === userId;
  }

  setExpandedUser(userId: string) {
    return window.localStorage.setItem('expanded', userId);
  }

  private storeData() {
    this.users = [...this.users];
    window.localStorage.setItem('app-data', JSON.stringify(this.users));
  }

  private getData() {
    const storedData = window.localStorage.getItem('app-data');
    if (!storedData) {return;}
    this.users = JSON.parse(storedData);
  }
}

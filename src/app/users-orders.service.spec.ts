import { TestBed, inject } from '@angular/core/testing';

import { UsersOrdersService } from './users-orders.service';

describe('UsersOrdersService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UsersOrdersService]
    });
  });

  it('should be created', inject([UsersOrdersService], (service: UsersOrdersService) => {
    expect(service).toBeTruthy();
  }));
});

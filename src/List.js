"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
exports.__esModule = true;
var _ = require("lodash");
function guid() {
    var s4 = function () { return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1); };
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}
var LIstItem = /** @class */ (function () {
    function LIstItem(name, _id) {
        if (_id === void 0) { _id = null; }
        this.name = name;
        this._id = _id;
        if (typeof this._id !== 'number') {
            this._id = guid();
        }
    }
    return LIstItem;
}());
exports.LIstItem = LIstItem;
var List = /** @class */ (function (_super) {
    __extends(List, _super);
    function List() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    List.prototype.add = function (item) {
        this.push(item);
    };
    List.prototype.removeByValue = function (value) {
        _.remove(this, function (item) { return item._id === value; });
    };
    List.prototype.removeById = function (_id) {
        _.remove(this, function (item) { return item._id === _id; });
    };
    List.prototype.edit = function (value, newItem) {
        var index = _.findIndex(this, function (item) { return item._id === value; });
        this[index] = __assign({}, this[index], newItem);
    };
    return List;
}(Array));
var users = new List();
users.add('a');
